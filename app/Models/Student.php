<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';


    public function course()
	{
		return $this->belongsTo('App\Models\Course');
	}

	public function inst()
	{
		return $this->belongsTo('App\Models\Instructors', 'instructors_id', 'id');
	}
}


