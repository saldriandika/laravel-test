<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table = 'payment';

    public function std()
    {
    	return $this->belongsTo('App\Models\Student','student_id','id');
    }
}
