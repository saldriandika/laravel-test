<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Course;
use Session;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
        $course = Course::orderBy('created_at', 'asc')->get();;

        return view('course.index', compact('course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
    
        $this->validate($r, ['name' => 'required']);
        
            $course = new Course();
            $course->name = $r->name;
            $course->description = $r->isi;
            $course->save();

            if($course)
                Session::flash('pesan', 'Course Created!');
            return redirect('/course');    
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $e = Course::findOrFail($id);
        return view('course.edit', compact('e'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $this->validate($r, ['name' => 'required']);
        
            $course = Course::findOrFail($id);
            $course->name = $r->name;
            $course->description = $r->isi;
            $course->save();

            if($course)
                Session::flash('pesan', 'Course Updated!');
            return redirect('/course');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $course = Course::destroy($id);
        if($course)
            Session::flash('pesan', 'Course Deleted!');
        return redirect('/course');
    }
}
