<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Instructors;
use Session;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
        $inst = Instructors::orderBy('created_at', 'asc')->get();

        return view('instructor.index', compact('inst'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('instructor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
    
        $this->validate($r, ['name' => 'required','gender' => 'required']);
        
            $course = new Instructors();
            $course->name = $r->name;
            $course->gender = $r->gender;
            $course->save();

            if($course)
                Session::flash('pesan', 'Instructors Created!');
            return redirect('/instructor');    
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $e = Instructors::findOrFail($id);
        return view('instructor.edit', compact('e'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $this->validate($r, ['name' => 'required','gender' => 'required']);
        
            $course = Instructors::findOrFail($id);
            $course->name = $r->name;
            $course->gender = $r->gender;
            $course->save();

            if($course)
                Session::flash('pesan', 'Instructors Updated!');
            return redirect('/instructor');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $course = Instructors::destroy($id);
        if($course)
            Session::flash('pesan', 'Instructors Deleted!');
        return redirect('/instructor');
    }
}
