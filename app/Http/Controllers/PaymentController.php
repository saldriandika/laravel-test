<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Payments;
use App\Models\Student;
use Session;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $pay = Payments::orderBy('created_at', 'asc')->get();

        return view('payment.index', compact('pay'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $std = Student::all();
        return view('payment.create', compact('std'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $rand = date('dmYHism').mt_rand(11111, 99999);
        $this->validate($r, ['amount' => 'required','status' => 'required']);
        
            $pay = new Payments();
            $pay->code = 'CD'.$rand;
            $pay->student_id = $r->std;
            $pay->amount = $r->amount;
            $pay->status = $r->status;
            $pay->save();

            if($pay)
                Session::flash('pesan', 'Payment Success!');
            return redirect('/payment');    
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $e = Payments::findOrFail($id);
        $std = Student::all();
        return view('payment.edit', compact('e','std'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $this->validate($r, ['amount' => 'required','status' => 'required']);
        
            $pay = Payments::findOrFail($id);
            $pay->student_id = $r->std;
            $pay->amount = $r->amount;
            $pay->status = $r->status;
            $pay->save();

            if($pay)
                Session::flash('pesan', 'Payment Update Success!');
            return redirect('/payment'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pay = Payments::destroy($id);
        if($pay)
            Session::flash('pesan', 'Payment Deleted!');
        return redirect('/payment');
    }
}
