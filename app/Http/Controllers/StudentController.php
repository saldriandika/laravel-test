<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use Validator;
use DB;
use App\User;
use App\Role;
use App\Permission;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\Models\Student;
use App\Models\Course;
use App\Models\Instructors;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //$hasil = User::all();
        //$hasil = Role::first()->users()->get();

        $hasil = Student::with('course','inst')->orderBy('created_at', 'asc')->get();
        // dd($hasil);
        return view('student.index')->with('std',$hasil);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $inst = Instructors::all();
        $crs = Course::all();
        return view('student.daftar', compact(['inst','crs']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validasi data
        $v = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:students',
            'gender' => 'required',
            'course_id' => 'required',
            'inst_id' => 'required',
            'email' => 'required|email|max:255|unique:students',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6'

        ]);

        if ($v->fails()) {
            return redirect('student/create')
                        ->withErrors($v)
                        ->withInput();
        }

        // dd($request);

        $password = $request->password;
        $password = bcrypt($password);

        $user = new Student;
        $user->email = $request->email;
        $user->course_id = $request->course_id;
        $user->Instructors_id = $request->inst_id;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->active = $request->active;
        $user->password = $password;
        $user->save();

        return redirect('student')
            ->with('pesan','Student Created !');
    }

   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $inst = Instructors::all();
        $crs = Course::all();
         $h = Student::findOrFail($id);
        //return $hasil;
        return view('student.edit')
                    ->with('e',$h)
                    ->with('crs',$crs)
                    ->with('inst',$inst);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         $v = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'gender' => 'required',
            'course_id' => 'required',
            'inst_id' => 'required',

        ]);

        if ($v->fails()) {
            return redirect('student/'.$id.'/edit')
                        ->withErrors($v)
                        ->withInput();
        }

        // dd($request);

        $password = $request->password;
        $password = bcrypt($password);

        $user = Student::findOrFail($id);
        $user->email = $request->email;
        $user->course_id = $request->course_id;
        $user->Instructors_id = $request->Inst_id;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->active = $request->active;
        $user->save();

        return redirect('/student')
            ->with('pesan','Student Updated!');
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $user = student::destroy($id);
        if($user)
            Session::flash('pesan', 'Student Deleted!');
        return redirect('/student');
    }
}
