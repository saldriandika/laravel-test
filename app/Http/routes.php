<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'StudentController@index');

Route::auth();

Route::get('student', 'StudentController@index');
Route::get('student/create', 'StudentController@create');
Route::post('student/create', 'StudentController@store');
Route::get('student/{id}/edit', 'StudentController@edit');
Route::post('student/{id}/edit', 'StudentController@update');
Route::get('student/{id_user}/delete', 'StudentController@destroy');


Route::get('course','CourseController@index');
Route::get('course/create','CourseController@create');
Route::post('course/create','CourseController@store');
Route::get('course/{id}/edit','CourseController@edit');
Route::post('course/{id}/edit','CourseController@update');
Route::get('course/{id}/delete','CourseController@destroy');

Route::get('instructor','InstructorController@index');
Route::get('instructor/create','InstructorController@create');
Route::post('instructor/create','InstructorController@store');
Route::get('instructor/{id}/edit','InstructorController@edit');
Route::post('instructor/{id}/edit','InstructorController@update');
Route::get('instructor/{id}/delete','InstructorController@destroy');


Route::get('payment','PaymentController@index');
Route::get('payment/create','PaymentController@create');
Route::post('payment/create','PaymentController@store');
Route::get('payment/{id}/edit','PaymentController@edit');
Route::post('payment/{id}/edit','PaymentController@update');
Route::get('payment/{id}/delete','PaymentController@destroy');