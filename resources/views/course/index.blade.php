
@extends('welcome')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
 
  <!-- Main content -->
  <section class="content container-limited">


    <div class="row">
      <div class="col-md-12">
        @if(Session::has('pesan'))    
        <div class="alert alert-success alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-check"></i> Sukses!</h4>
          {{ Session::get('pesan') }}
        </div>
        @endif

        @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-ban"></i> Gagal!</h4>
          <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
         <section class="content-header">
          <div class="header">
            <legend>List Course</legend>
            <ol class="breadcrumb">
              <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Course</li>
            </ol>
          </div>
        </section>

        <div class="box box-solid">
          <div class="box-body">
            <a href="{{URL::to('course/create')}}"><button class="btn btn-sm btn-info"><span class="glyphicon glyphicon-user"></span> Create Course</button></a><br><br>
            <table id="table" class="table table-bordered table-hover">
                  <thead>
                      <tr>
                          <th>Name</th>
                          <th>Description</th>
                          <th width="30px">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($course as $c)
                      <tr>
                          <td>{{ $c->name }}</td>
                          <td>{{ $c->description }}</td>
                          <td class="nw">
                              <a class="btn btn-xs btn-warning" href="{{URL::to('course/'.$c->id.'/edit/')}}" data-toltip="tooltip"  data-placement="left" title="Edit data"><span class="glyphicon glyphicon-edit"></span></a>
                              <a class="btn btn-xs btn-danger" href="{{URL::to('course/'.$c->id.'/delete')}}" onclick="return confirm('Are you sure?')" data-toltip="tooltip"  data-placement="left" title="Delete data"><span class="glyphicon glyphicon-trash"></span></a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection
