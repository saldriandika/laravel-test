
@extends('welcome')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <!-- Main content -->
  <section class="content container-limited">

    @if(Session::has('pesan'))    
    <div class="alert alert-success alert-dismissable">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      <h4><i class="icon fa fa-check"></i> Sukses!</h4>
      {{ Session::get('pesan') }}
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      <h4><i class="icon fa fa-ban"></i> Gagal!</h4>
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

    <div class="row">
      <div class="col-md-12">
        <section class="content-header">
          <div class="header">
            <legend>Create Payment</legend>
          </div>
          <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Payment</li>
          </ol>
        </section>

        <div class="box box-solid">
            <form id="idform" action="{{url('payment/create')}}"  method="POST" >
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="box-body">

                <div class="form-group">
                  <label for="gender">* Student</label>
                  <select class="form-control" name="std">
                    @foreach($std as $s)
                      <option value="{{ $s->id }}">{{ $s->name }} / {{ $s->gender }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group">
                  <label for="name">* Amount</label>
                  <input type="text" name="amount" placeholder="Amount"  class="form-control">
                </div>

                <div class="form-group">
                  <label for="gender">* Status</label>
                  <select class="form-control" name="status">
                    <option value="done">Done</option>
                    <option value="clear">Clear</option>
                  </select>
                </div>

              </div>

              <div class="box-footer">
                <button class="btn btn-primary pull-right" id='simpan' type="submit"><span class="glyphicon glyphicon-save"></span> Simpan</button>
                <a href="{{URL::to('/')}}" class="btn btn-success"><span class="glyphicon glyphicon-arrow-left"></span> Kembali</a>
              </div>
            </form>
        </div>
      </div>
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection