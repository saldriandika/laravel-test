<html>
    <head>
        <title>Laravel test</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cerulean/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('/datatables/dataTables.bootstrap.css') }}">

    </head>
    <body>
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="{{ url('/logout') }}" class="navbar-brand">Laravel test</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        @if (!Auth::guest())
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li>
              <a  href="{{ url('/student') }}" >Student </span></a>
            </li>
            <li>
              <a  href="{{ url('/course') }}" >Course </span></a>
            </li>
            <li>
              <a  href="{{ url('/instructor') }}" >Instructors </span></a>
            </li>
            <li>
              <a  href="{{ url('/payment') }}" >Payments </span></a>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Hy, {{ ucwords(Auth::user()->name) }}</a></li>
            <li><a href="{{ url('/logout') }}">Logout</a></li>
          </ul>
          @else
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('/login') }}" target="_blank">Login</a></li>
          </ul>
          @endif
        </div>
      </div>
    </div>


    <div class="container">
    <br><hr>
    @yield('content')
       

      <!-- <footer>
        <div class="row">
          <div class="col-lg-12">
            <p>Based on <a href="http://getbootstrap.com" rel="nofollow">Bootstrap</a>. Icons from <a href="http://fortawesome.github.io/Font-Awesome/" rel="nofollow">Font Awesome</a>. Web fonts from <a href="http://www.google.com/webfonts" rel="nofollow">Google</a>.</p>

          </div>
        </div>

      </footer> -->


    </div>
    <script src="{{ asset('/jQuery/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ asset('/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript">
    $(document).ready(function(){
        $("#table").DataTable({
          "order": [[2, 'asc']],
           "lengthMenu": [ [5, 100, 500, -1], [50, 100, 500, "All"] ],
           "columnDefs": [ { "orderable": true, "targets": [1] } ],
           "language": {"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Indonesian.json"}
        });
    })
    </script>
</body>
</html>
