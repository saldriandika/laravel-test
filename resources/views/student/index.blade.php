
@extends('welcome')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
 
  <!-- Main content -->
  <section class="content container-limited">


    <div class="row">
      <div class="col-md-12">
        @if(Session::has('pesan'))    
        <div class="alert alert-success alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-check"></i> Sukses!</h4>
          {{ Session::get('pesan') }}
        </div>
        @endif

        @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-ban"></i> Gagal!</h4>
          <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
         <section class="content-header">
          <div class="header">
            <legend>List Student</legend>
            <ol class="breadcrumb">
              <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">List Student</li>
            </ol>
          </div>
        </section>

        <div class="box box-solid">
          <div class="box-body">
            <a href="{{URL::to('student/create')}}"><button class="btn btn-sm btn-info"><span class="glyphicon glyphicon-user"></span> Create User</button></a><br><br>
            <table id="table" class="table table-responsive table-bordered table-striped">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Gender</th>
                  <th>Course</th>
                  <th>Instructor</th>
                  <th>Active</th>
                  <th style="width:110px !important">Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($std as $h)
                <tr>
                  <td>{{ $h->name }}</td>
                  <td>{{ $h->email }}</td>
                  <td>{{ $h->gender }}</td>
                  <td>{{ $h->course['name'] }}</td>
                  <td>{{ $h->inst['name'] }}</td>
                  <td>{{ ($h->active == 0) ? 'None Active'  : 'Active'}}</td>
                  <td>
                    <a class="btn btn-xs btn-warning" href="{{URL::to('student/'.$h->id.'/edit')}}" data-toltip="tooltip"  data-placement="left" title="Edit data"><span class="glyphicon glyphicon-edit"></span></a>
                    <a class="btn btn-xs btn-danger" href="{{URL::to('student/'.$h->id.'/delete')}}" onclick="return confirm('Are you sure?')" data-toltip="tooltip"  data-placement="left" title="Delete data"><span class="glyphicon glyphicon-trash"></span></a>

                    
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection