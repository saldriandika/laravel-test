
@extends('welcome')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <!-- Main content -->
  <section class="content container-limited">

    @if(Session::has('pesan'))    
    <div class="alert alert-success alert-dismissable">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      <h4><i class="icon fa fa-check"></i> Sukses!</h4>
      {{ Session::get('pesan') }}
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      <h4><i class="icon fa fa-ban"></i> Gagal!</h4>
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

    <div class="row">
      <div class="col-md-12">
        <section class="content-header">
          <div class="header">
            <legend>Edit Student</legend>
          </div>
          <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Student</li>
          </ol>
        </section>

        <div class="box box-solid">
            <form id="idform" action="{{url('student/'.$e->id.'/edit')}}"  method="POST" >
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="box-body">

                <div class="form-group">
                  <label for="name">* Name</label>
                  <input type="text" name="name" placeholder="Name" value="{{ $e->name }}" class="form-control">
                </div>

                <div class="form-group">
                  <label for="name">* Email</label>
                  <input type="email" name="email" placeholder="Email"  value="{{ $e->email }}" class="form-control">
                </div>

                <div class="form-group">
                  <label for="gender">* Gender</label>
                  <select class="form-control" name="gender">
                    <option <?= ($e->gender == 'men') ? 'selected' : '';?> value="men">Men</option>
                    <option <?= ($e->gender == 'women') ? 'selected' : '';?> value="Women">Women</option>
                  </select>
                </div>

                <div class="form-group">
                  <label for="course">* Course</label>
                  <select class="form-control" name="course_id">
                    @foreach($crs as $c)
                      <option <?= ($c->id == $e->course_id) ? 'selected' : '';?> value="{{ $c->id }}">{{ $c->name }} / {{ $c->description }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group">
                  <label for="inst">* Instructors</label>
                  <select class="form-control" name="inst_id">
                    @foreach($inst as $i)
                      <option <?= ($i->id == $e->instructors_id) ? 'selected' : '';?> value="{{ $i->id }}">{{ $i->name }} / {{ $i->gender }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group">
                  <label for="gender">* Active</label>
                  <select class="form-control" name="active">
                    <option <?= ($e->active == 0) ? 'selected' : '';?> value="0">Un-active</option>
                    <option  <?= ($e->active == 1) ? 'selected' : '';?>  value="1">Active</option>
                  </select>
                </div>

                <!-- <div class="form-group">
                  <label for="keterangan">* Isi Berita</label>
                  <textarea class="form-control" name="isi"></textarea>
                </div> -->

              </div>

              <div class="box-footer">
                <button class="btn btn-primary pull-right" id='simpan' type="submit"><span class="glyphicon glyphicon-save"></span> Update</button>
                <a href="{{URL::to('/')}}" class="btn btn-success"><span class="glyphicon glyphicon-arrow-left"></span> Kembali</a>
              </div>
            </form>
        </div>
      </div>
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection