
@extends('layouts.bootwatch')

@section('content')    
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="header">
      <legend>Edit Member</legend>
    </div>
    <ol class="breadcrumb">
      <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Setting User</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content container-limited">

    <div class="row">
      <div class="col-md-12">
         @if(Session::has('pesan'))    
        <div class="alert alert-success alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-check"></i> Sukses!</h4>
          {{ Session::get('pesan') }}
        </div>
        @endif
        
        @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-ban"></i> Gagal!</h4>
          <ul class="list-unstyled">
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
        </div>
        @endif
        <div class="box box-solid">
          <div class="panel panel-default">
            <div class="panel-body">
              <form id="idform" action="{{url('setting/user/'.$h->id.'/edit')}}"  method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">

                  <div class="col-md-6">
                      <div class="form-group">
                        {!!Form::label('username', 'Username')!!}
                        {!!Form::text('username',$h->username, array('class' => 'form-control','placeholder'=>'Username'))!!}
                      </div>

                      <div class="form-group">
                        {!!Form::label('name', 'Nama')!!}
                        {!!Form::text('name',$h->name, array('class' => 'form-control','placeholder'=>'Nama Lengkap'))!!}
                      </div> 

                      <div class="form-group">
                        {!!Form::label('admin', 'Alamat')!!}
                          <div class="row">
                              <div class="col-md-6">
                                <select class="form-control" name="daerah">
                                  <option value="">.:: PIlih Daerah ::.</option>
                                  @foreach($daerah as $l)
                                    <option value="{{ $l->id }}" <?= ($l->id == $h->daerah_id) ? 'selected' : '';?>>{{ $l->nama_daerah }} ({{ $l->kota }})</option>
                                  @endforeach
                                </select>
                              </div>
                              <div class="col-md-6">  
                                {!!Form::text('alamat',$h->alamat, array('class' => 'form-control','placeholder'=>'Alamat Specifik : Desa, Rt, Rw..dsb'))!!}
                              </div>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        {!!Form::label('pekerjaan', 'Pekerjaan')!!}
                        {!!Form::text('pekerjaan',$h->pekerjaan, array('class' => 'form-control','placeholder'=>'Pekerjaan'))!!}
                      </div>

                      <div class="form-group">
                        {!!Form::label('contact', 'No Telp / Hp')!!}
                        {!!Form::text('contact',$h->contact, array('class' => 'form-control','placeholder'=>' No Telp / Hp'))!!}
                      </div> 

                      <div class="form-group">
                        {!!Form::label('email', 'Alamat Email')!!}
                        {!!Form::text('email',$h->email, array('class' => 'form-control','placeholder'=>' Alamat Email'))!!}
                      </div> 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="name">* Level</label>
                        <select name="role" class="form-control">
                          <option value="">.:Pilih Level:.</option>
                          <option value="3">Petugas</option>
                          <option value="2">Member</option>
                        </select>
                      </div>
                      <div class="box-footer">
                        <button class="btn btn-primary pull-right" id='simpan' type="submit"><span class="glyphicon glyphicon-save"></span> Simpan</button>
                        <a href="{{URL::to('/user')}}" class="btn btn-success"><span class="glyphicon glyphicon-arrow-left"></span> Kembali</a>
                      </div>
                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section ('js')
<script type="text/javascript">

  var option = {
    framework: 'bootstrap',
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      'name': {
        validators: {
          notEmpty: {
            message: 'Harus Di Isi'
          }
        }
      },
      'role': {
        validators: {
          notEmpty: {
            message: 'Harus Di Isi'
          }
        }
      },
      'email': {
        validators: {
          notEmpty: {
            message: 'Harus Di Isi'
          },
          emailAddress: {
            message: 'Email tidak valid'
          }
        }
      }
    }
  };

  $('#idform').formValidation(option);

</script>
@endsection
